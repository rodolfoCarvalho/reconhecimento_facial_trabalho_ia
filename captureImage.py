# -*- encoding: utf-8 -*-
import cv2 as cv
import numpy as np
import csv
import time
import os
import copy
import sklearn
import random
from random import randint
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import Perceptron
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score

"""
TRABALHO 1 - INTELIGÊNCIA ARTIFICIAL

Este script é responsável por tirar fotos e salválas como um conjunto
de treinamento para o trabalho de reconhecimento facial.

@author Daniel de Faria Godoi
@author Rodolfo Borges
"""
#Essa função tem o objetivo de verificar a movimentação do mouse
def draw(event, m, n, flags, param):
    global x, y
    if event == cv.EVENT_MOUSEMOVE:
        x = m
        y = n

#Essa função tem o objetivo de capturar as imagens e armazená-las
def photograph(capture, path, name, csvfile, k, happy):
    #time.sleep(3)
    global x, y, finish
    key_enter1 = 10 #tecla enter ubuntu
    key_enter2 = 13 #tecla enter windows
    key_finish1 = 113 #tecla q
    key_finish2 = 81 #tecla Q
    x = int(capture.get(3) / 2)
    y = int(capture.get(4) / 2)
    while k <= 50:
        #Verificar posição do Mouse
        cv.setMouseCallback('Frame', draw)
        #Capturar imagem da WebCam
        retval, image = capture.read()
        image = cv.flip(image, 1)
        copy_image = copy.deepcopy(image)
        #Calcular posição do retânfulo e de seus elementos
        a = x - 120
        b = x + 120
        c = y - 120
        d = y + 120    
        ponto_a = (a, c) #(x, y)
        ponto_b = (b, d) #(x, y)
        center = (x, y)
        bottom = (x - 121, y + 140)
        l1 = (x, y - 10)
        l2 = (x, y + 10)
        l3 = (x - 10, y)
        l4 = (x + 10, y)
        #Desenhar o retângulo e seus elementos
        cv.rectangle(copy_image, ponto_a, ponto_b, (0, 255, 0), 1, lineType = cv.CV_AA)
        cv.circle(copy_image, center, 15, (255, 0, 0), 1, lineType = cv.CV_AA)
        cv.line(copy_image, l1, l2, (0, 255, 0), 1, lineType = cv.CV_AA)
        cv.line(copy_image, l3, l4, (0, 255, 0), 1, lineType = cv.CV_AA)
        cv.putText(copy_image, "%d" % (50 - k + 1), bottom, cv.FONT_HERSHEY_SIMPLEX, .75, (255, 0, 0), thickness = 2, lineType = cv.CV_AA)
        #Gerar a imagem capurada da webcam para visualização do usuário através da janela nomeada como 'Frame'
        cv.imshow("Frame", copy_image)
        #Capturar imagem delimitada pelo retângulo e guardá-la caso 'enter' seja clicado
        key = cv.waitKey(50) % 256
        if a > 0 and c > 0 and d < int(capture.get(4)) and b < int(capture.get(3)) and (key == key_enter1 or key == key_enter2):
            #Tranformar imagem colorida em tons de cinza para armazenamento
            image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
            crop_image = image[c:d, a:b] #y, x    
            imageResized = cv.resize(crop_image, (64, 64), fx = 0, fy = 0)
            if happy == 1:
                cv.imwrite("%s/%s/photos/happy/happy%d.jpg" % (path, name, k), imageResized)
            if happy == 0:
                cv.imwrite("%s/%s/photos/sad/sad%d.jpg" % (path, name, k), imageResized)
            k += 1
            vector = []
            for i in range(64):
                for j in range(64):
                    vector.append(imageResized[i][j])
            writer = csv.writer(csvfile, lineterminator = '\n')
            writer.writerow(vector + [happy])
        #Finalizar modo de captura caso 'q' seja clicado
        elif key == key_finish1 or key == key_finish2:
            k=100
            finish=1
    cv.destroyWindow("Frame")
    cv.waitKey(10)

#Essa função tem o objetivo de criar diretórios para organização e armazenamento das fotos capturadas   
def makedir(path, name):
    if not (os.path.isdir(path)):
        os.mkdir(path);
    if not (os.path.isdir(path + "/" + name)):
        os.mkdir(path + "/" + name)
        os.mkdir(path + "/" + name + "/photos/")
        os.mkdir(path + "/" + name + "/photos/happy")
        os.mkdir(path + "/" + name + "/photos/sad")

#Essa função tem o objetivo de capturar as fotos a serem usadas como treinamento  
def training():
    global finish
    key_finish = 0
    #Inicia o objeto usado para captura das imagens
    capture = cv.VideoCapture(0)
    #Criar diretórios de armazenamento usando um nome fornecido pelo usuário
    path = "data"
    print( "Digite o seu nome completo: " )
    name = raw_input()
    makedir(path, name)
    #Instruções de utilização do programa

    print( u"Atenção:" )
    print( u"Clique na janela da imagem quando ela abrir" )
    print( u"Ajuste seu rosto no retângulo e alinhe seu nariz na circunferência" )
    print( u"Dê enter para capaturar a imagem!" )
    print( u"Pressione 'q' para sair do modo de captura." )
    #time.sleep(10)
    #Criação do arquivo para armazenamento do conteúdo das imagens
    csvfile = open('%s/%s/%s.csv' % (path, name, name), 'w')
    #Iniciar a captura das fotos
    print( )
    print( u"PESSOA FELIZ :)" )
    if finish==0:
        photograph(capture, path, name, csvfile, 1, 1)
    print( )
    print( u"PESSOA TRISTE :(" )
    if finish==0:
        photograph(capture, path, name, csvfile, 1, 0)
    #Destruir o objeto usado para captura das fotos
    capture.release()
def cheching_webcam(assay):
    global x, y
    capture = cv.VideoCapture(0)
    key_finish1 = 113 #tecla q
    key_finish2 = 81 #tecla Q
    x = int(capture.get(3) / 2)
    y = int(capture.get(4) / 2)
    valid = 1
    happy = 2
    while valid:
        #Verificar posição do Mouse
        cv.setMouseCallback('Frame', draw)
        #Capturar imagem da WebCam
        retval, image = capture.read()
        image = cv.flip(image, 1)
        copy_image = copy.deepcopy(image)
        #Calcular posição do retânfulo e de seus elementos
        a = x - 120
        b = x + 120
        c = y - 120
        d = y + 120    
        ponto_a = (a, c) #(x, y)
        ponto_b = (b, d) #(x, y)
        center = (x, y)
        bottom = (x - 121, y + 140)
        l1 = (x, y - 10)
        l2 = (x, y + 10)
        l3 = (x - 10, y)
        l4 = (x + 10, y)
        #Desenhar o retângulo e seus elementos
        cv.rectangle(copy_image, ponto_a, ponto_b, (0, 255, 0), 1, lineType = cv.CV_AA)
        cv.circle(copy_image, center, 15, (255, 0, 0), 1, lineType = cv.CV_AA)
        cv.line(copy_image, l1, l2, (0, 255, 0), 1, lineType = cv.CV_AA)
        cv.line(copy_image, l3, l4, (0, 255, 0), 1, lineType = cv.CV_AA)
        if happy==1:
            cv.putText(copy_image, "FELIZ", bottom, cv.FONT_HERSHEY_SIMPLEX, .75, (255, 0, 0), thickness = 2, lineType = cv.CV_AA)
        elif happy==0:
            cv.putText(copy_image, "TRISTE", bottom, cv.FONT_HERSHEY_SIMPLEX, .75, (255, 0, 0), thickness = 2, lineType = cv.CV_AA)
        #Gerar a imagem capurada da webcam para visualização do usuário através da janela nomeada como 'Frame'
        cv.imshow("Frame", copy_image)
        #Capturar imagem delimitada pelo retângulo e guardá-la caso 'enter' seja clicado
        key = cv.waitKey(50) % 256
        #Finalizar modo de captura caso 'q' seja clicado
        if key == key_finish1 or key == key_finish2:
            valid=0
        elif a > 0 and c > 0 and d < int(capture.get(4)) and b < int(capture.get(3)):
            #Tranformar imagem colorida em tons de cinza para armazenamento
            image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
            crop_image = image[c:d, a:b] #y, x    
            imageResized = cv.resize(crop_image, (64, 64), fx = 0, fy = 0)
            num=0
            for i in range(64):
                for j in range(64):
                    if num==0:
                        vector = [int(imageResized[i][j])]
                    else:
                        vector.extend([int(imageResized[i][j])])
                    num = num + 1
            happy = assay.predict(vector)
    cv.destroyWindow("Frame")
    cv.waitKey(10)
    capture.release()
def checking_tests(assay,test_paths):
    size = len(test_paths)
    while size:
        name = test_paths.pop(0)
        csvfile = open('data/%s/%s.csv' % (name, name), 'r')
        reader = csv.reader(csvfile)
        cont = 0
        for row in reader:
            row = [ int(x) for x in row ]
            happy = row.pop()
            num = assay.predict([row])
            if cont==0:
                array_predicted = [num]
                array_correct = [happy]
            else:
                array_predicted.extend([num])
                array_correct.extend([happy])   
            #print( array_x )
            cont = cont + 1
        size = size - 1
        csvfile.close()
        print( "\n%s:\nPrecisão: %f\nRecall: %f\nF1: %f\n" % (name,precision_score(array_correct, array_predicted, average='weighted'),recall_score(array_correct, array_predicted, average='weighted'),f1_score(array_correct, array_predicted, average='weighted')) )


def checking_base():
    if not (os.path.isdir("data")):
        print( "Inicie alguns conjuntos de treinamentos!" )
        return 0
    test_paths = os.listdir("data")
    size = len(test_paths)
    if size == 0:
        print( "Inicie alguns conjuntos de treinamentos!" )
        return 0
    valid = 0
    print( "Qual algoritmo você gostaria de usar ?\nÁvore de decisão(1)\nKNN(2)\nRedes Neurais(3)" )
    while valid==0:
        type_t = int(raw_input())
        if type_t < 1 or type_t > 3:
            print( "Resposta inválida!" )
        else:
            valid=1
    print( "\nVocê tem %d treinameno(s) realizado(s).\n" % (size) )
    valid=0
    print( "Informe o número de treinamentos a serem utilizados:" )
    while valid==0 :
        tests = int(raw_input())
        if tests <= size:
            valid=1
    cont = 0
    while tests:
        name = test_paths.pop(0)
        csvfile = open('data/%s/%s.csv' % (name, name), 'r')
        reader = csv.reader(csvfile)
        for row in reader:
            row = [ int(x) for x in row ]
            happy = row.pop()
            if cont==0:
                array_x = [row]
                array_y = [happy]
            else:
                array_x.extend([row])
                array_y.extend([happy])
            #print( array_x )
            cont = cont + 1
        tests = tests - 1
        size = size - 1
        csvfile.close()
    if type_t==1:
        assay = DecisionTreeClassifier()
    elif type_t==2: 
        assay = KNeighborsClassifier()
    else:
        assay = Perceptron(n_iter=300)
    assay.fit(array_x,array_y)
    valid = 0
    num = 2
    if size>0:
        print( "Você deseja usar os treinamentos restantes (1) ou usar sua webcam (2) para os testes ?" )
        while valid==0:
            num = int(raw_input())
            if num < 1 or num > 2:
                print( "Resposta inválida!" )
            else:
                valid=1
    if num==1:
        checking_tests(assay,test_paths)
    else:
        cheching_webcam(assay)
def main():
    valid = 0
    print( "Ola, você gostaria de inserir novos treinamentos (1) ou iniciar os testes (2) ?" )
    while valid==0:
        num = int(raw_input())
        if num < 1 or num > 2:
            print( "Resposta inválida!" )
        else:
            valid=1
    if num == 1:
        training()
    else:
        checking_base()

#Iniciar variaveis globais
x = 0
y = 0
finish = 0
#Chamar a função principal
main()